<?php
namespace Micro;
use \Doctrine\ORM\EntityManager;

class EntityManager{
   public $entityManager;
   public function __construct(EntityManager $entityManager)
   {
       $this->entityManager = $entityManager;
   }
   
   public function getEntity()
   {
       return $this->entityManager;
   }
}

