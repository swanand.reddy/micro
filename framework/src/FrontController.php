<?php

namespace Micro;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing;
use Symfony\Component\HttpKernel;
use Symfony\Component\EventDispatcher\EventDispatcher;

use Doctrine\ORM\EntityManager;
/**
 * Main controller fetching the controllers
 */
class FrontController
{
    /**
     * This application request.
     *
     * @var Request
     */
    protected $request = null;

    /**
     * This application response.
     *
     * @var Response
     */
    public $response = null;

    /**
     * This application middlewares to be called after generating the response.
     *
     * @var array
     */
    protected $middlewareStack = array();

    /**
     * This application router
     * 
     */
    protected $router = null;
    
    protected $routes = null;
    
    /**
     * fetch request, dispatch response
     * 
     * @param array $route
     */
    public function __construct($route = [])
    {
        $this->request = Request::createFromGlobals();

        $this->router  = new Router();
        $this->routes  = $this->router->addRoutes($route);
        $this->context = new Routing\RequestContext();
        $this->matcher = new Routing\Matcher\UrlMatcher($this->routes, $this->context);
        
        $this->resolver = new HttpKernel\Controller\ControllerResolver();

        $this->dispatcher = new EventDispatcher();
        $this->dispatcher->addSubscriber(new ContentLengthListener());
        $this->dispatcher->addSubscriber(new AuthorListener());
                       
        $this->kernel = new Kernel($this->dispatcher, $this->matcher, $this->resolver);
        $this->response = $this->kernel->handle($this->request);
        
    }

}
