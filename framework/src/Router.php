<?php
namespace Micro;
use Symfony\Component\Routing;

class Router{
    protected $routes;
    public function __construct()
    {
         $this->routes = new Routing\RouteCollection();
    }
    
    public function addRoutes($routeArr=[])
    {
        foreach($routeArr as $route){
            $this->routes->add($route['name'], new Routing\Route($route['path'], $route['controlArg']));
        }
        return $this->routes;
    }
}