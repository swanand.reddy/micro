<?php
namespace Micro;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Matcher\UrlMatcherInterface;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\HttpKernel\Controller\ControllerResolverInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
/**
 * interpret the uri, retrieve the controller, dispatch event to response
 */
class Kernel
{
    private $matcher;
    private $resolver;
    private $dispatcher;
    /**
     * Match the route, get the controller based on the arguments
     * @param EventDispatcher $dispatcher
     * @param UrlMatcherInterface $matcher
     * @param ControllerResolverInterface $resolver
     */
    public function __construct(EventDispatcher $dispatcher, UrlMatcherInterface $matcher, ControllerResolverInterface $resolver)
    {
        $this->matcher = $matcher;
        $this->resolver = $resolver;
        $this->dispatcher = $dispatcher;
    }
    /**
     * handle the request
     * @param Request $request
     * @return Response
     */
    public function handle(Request $request)
    {
        $this->matcher->getContext()->fromRequest($request);

        try {      
           
            $request->attributes->add($this->matcher->match($request->getPathInfo()));
          
            $controller = $this->resolver->getController($request);
            
            $arguments = $this->resolver->getArguments($request, $controller);
                      
            $response = call_user_func_array($controller, $arguments);
        } catch (ResourceNotFoundException $e) {
            $response = new Response('Not Found', 404);
        } catch (\Exception $e) {
            error_log($e->getMessage());
            $response = new Response('An error occurred', 500);
        }

        // dispatch a response event
        $this->dispatcher->dispatch('response', new ResponseEvent($response, $request));

        return $response;
    }
}