**A micro framework V-1.0**  
This is a first attempt of incorporating the following features in a PHP framework:  
- orm
- PSR4
- routing
- FrontController
- Bootstrap(with pre/post dispatch)
- MVC design pattern
- configuration (yaml and PHP array)
- templating views(blocs content)
