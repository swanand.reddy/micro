<?php
namespace Application\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Templating\PhpEngine;
use Symfony\Component\Templating\TemplateNameParser;
use Symfony\Component\Templating\Loader\FilesystemLoader;
use Application\Model\ProductModel;


/**
 * manage product 
 */
class ProductController
{   
    /**
     * list all products
     * @param Request $request 
     * @return Response
     */
    public function indexAction()
    {
        $allProd = ProductModel::listAll();
        $loader = new FilesystemLoader(__DIR__.'/../View/%name%');

        $templating = new PhpEngine(new TemplateNameParser(), $loader);

        $response = new Response($templating->render('product.php', array('data' => $allProd)));
        $response->setTtl(10);
        
        return $response;
    }
    
    public function newAction($name)
    {
        $prodMod  = new ProductModel();
        $product = $prodMod->insertNew($name);
        $response = !empty($product) ? "Created Product with ID " . $product->getId() : 'Error creating product';
        return new Response($response);
    }
}