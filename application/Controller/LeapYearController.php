<?php
namespace Application\Controller;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Templating\PhpEngine;
use Symfony\Component\Templating\TemplateNameParser;
use Symfony\Component\Templating\Loader\FilesystemLoader;

use Application\Model\LeapYear;

class LeapYearController
{
    public function indexAction($year)
    {
        $leapyear = new LeapYear();
        $resp = '';
        if ($leapyear->isLeapYear($year)) {
           // $response = new Response('Yep, this is a leap year!');
            $resp = 'Yep, this is a leap year!';
        } else {
            //$response = new Response('Nope, this is not a leap year.');
            $resp = 'Nope, this is not a leap year.';
        }
        
        $loader = new FilesystemLoader(__DIR__.'/../View/%name%');

        $templating = new PhpEngine(new TemplateNameParser(), $loader);

        $response = new Response($templating->render('yearpage.php', array('data' => $resp)));
        $response->setTtl(10);
        
        return $response;
    }
}



