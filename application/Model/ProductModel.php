<?php
namespace Application\Model;
use Entity\Product;

use Doctrine\ORM\Tools\Setup;
/**
 * manage the data of table products
 */
class ProductModel
{    
   private $entityManager; 
   public function __construct()
   {    
        // Create a simple "default" Doctrine ORM configuration for yaml Mapping
        $isDevMode = true;

        $config = Setup::createYAMLMetadataConfiguration(array(__DIR__."/../../config/yaml"), $isDevMode);
      
        // database configuration parameters
        $conn = [
            'driver' => 'pdo_sqlite',
            'path' =>  __DIR__.'/../../config/sqlite/db.sqlite',
        ];

        // obtaining the entity manager
        $this->entityManager = \Doctrine\ORM\EntityManager::create($conn, $config);
       
   } 
   /**
   * select all products
   */ 
   public function listAll()
   {
       return [
                ['id'=>1,'name'=>'TV'],
                ['id'=>2,'name'=>'Radio'],
                ['id'=>3,'name'=>'Computer'],
                ['id'=>4,'name'=>'Microwave'],
                ['id'=>5,'name'=>'Kettle'],
                ['id'=>6,'name'=>'Refrigerator'],
                ['id'=>7,'name'=>'Washing Machine']
       ];
   }
   
    /**
    * insert new product
    * @param type $name
    */
    public function insertNew($name)
    {
        $product = new Product();        
        $product->setName($name);
        
        $this->entityManager->persist($product);
        $this->entityManager->flush();
        return $product;
    }
}