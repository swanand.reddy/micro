<?php
/**
 * routing config
 */
return [
    [
       'name' => 'leap_year',
       'path'  => '/is_leap_year/{year}',
        'controlArg' => [
            'year' => null,
            '_controller' => 'Application\\Controller\\LeapYearController::indexAction',
        ]
    ],
    [
       'name' => 'newProd',
       'path'  => '/new_prod/{name}',
        'controlArg' => [
            'name' => null,
            '_controller' => 'Application\\Controller\\ProductController::newAction',
        ]
    ],
    [
       'name' => 'product',
       'path'  => '/all_prod',
        'controlArg' => [            
            '_controller' => 'Application\\Controller\\ProductController::indexAction',
        ]
    ],
];
