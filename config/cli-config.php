<?php
// Relying on autoloading magic thanks to Composer
require 'autoload.php';

//database orm
require 'database.php';

$helperSet = new \Symfony\Component\Console\Helper\HelperSet(array(
    'em' => new \Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper($entityManager)
));

return $helperSet;
