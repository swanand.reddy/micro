<?php
use Doctrine\ORM\Tools\Setup;
// Create a simple "default" Doctrine ORM configuration for yaml Mapping
$isDevMode = true;

$config = Setup::createYAMLMetadataConfiguration(array(__DIR__."/yaml"), $isDevMode);

// database configuration parameters
$conn = [
    'driver' => 'pdo_sqlite',
    'path' =>  'config/sqlite/db.sqlite',
];

// obtaining the entity manager
$entityManager = \Doctrine\ORM\EntityManager::create($conn, $config);