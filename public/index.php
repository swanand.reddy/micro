<?php
// Relying on autoloading magic thanks to Composer
require __DIR__.'/../config/autoload.php';

// Getting the application instance
$app = require __DIR__.'/../config/bootstrap.php';

$app->response->send();
